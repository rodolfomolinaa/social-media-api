<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('client.id');
        return [
            'name' => 'required',
            'email' => [
                'email',
                'required',
                "unique:clients,email,$id"
            ],
            'address' => 'required',
            'phone' => 'required',
            'logo_url' => 'required',
            'country_id' => 'required',
        ];
    }
}
