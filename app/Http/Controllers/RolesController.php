<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;

class RolesController extends Controller
{
    /**
     * @var Role
     */
    protected $role;

    /**
     * RolesController constructor.
     * @param Role $role
     */
    public function __construct(Role $role)
    {
        $this->role = $role;
    }


    public function select()
    {
        return $roles = DB::table('roles')
        ->select('id','name')
        ->where('name', '!=', 'Root')
        ->where('name', '!=', 'Cliente')->get();
    }
}
