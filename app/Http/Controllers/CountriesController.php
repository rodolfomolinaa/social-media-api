<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\CountryCollection;
use App\Models\Country;
use App\Http\Requests\Country\Store as StoreRequest;
use App\Http\Requests\Country\Update as UpdateRequest;

class CountriesController extends Controller
{
    /**
     * @var Country
     */
    protected $country;

    /**
     * CountriesController constructor.
     * @param Country $country
     */
    public function __construct(Country $country)
    {
        $this->country = $country;
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return CountryCollection
     */
    public function index(Request $request): CountryCollection
    {
        return new CountryCollection($this->country->with('clients')->paginate($request->input('per_page')));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return Country
     */
    public function store(StoreRequest $request): Country
    {
        return $this->country->create([
            'name' => $request->input('name'),
        ])->load('clients');
    }

    /**
     * Display the specified resource.
     *
     * @param Country $country
     * @return Country
     */
    public function show(Country $country): Country
    {
        return $country->load('clients');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param Country $country
     * @return Country
     */
    public function update(UpdateRequest $request, Country $country): Country
    {
        $country->update([
            'name' => $request->input('name')
        ]);
        return $country->load('clients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Country $country
     * @return Country
     */
    public function destroy(Country $country): Country
    {
        try {
            $country->delete();
        } catch (\Exception $exception) {
            return response([
                'message' => 'Cannot delete this country',
                'exception' => $exception
            ]);
        }
        return $country->load('clients');
    }

    public function select()
    {
        return $this->country->select('id', 'name')->get();
    }
}
