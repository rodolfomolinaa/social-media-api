<?php

namespace App\Http\Controllers;

use App\Mail\UserCreated;
use Illuminate\Http\Request;
use App\Http\Resources\UserCollection;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Http\Requests\User\Store as StoreRequest;
use App\Http\Requests\User\Update as UpdateRequest;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    /**
     * @var User
     */
    protected $user;

    /**
     * UsersController constructor.
     * @param User $user
     */
    public function  __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Usercollection
     */
    public function index(Request $request): UserCollection
    {
        // return auth()->user();
        // return new UserCollection(User::with('manager', 'users', 'roles')->latest()->paginate($request->input('per_page')));

        return new UserCollection(auth()->user()->users()->with('manager', 'users', 'roles')->latest()->paginate($request->input('per_page')));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest $request
     * @return User
     */
    public function store(StoreRequest $request)
    {
        $user = DB::transaction(function () use ($request) {
            $password = Str::random(10);
            $user = $this->user->create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => $password,
            ])->assignRole($request->input('role_id'));

            $user->manager()->attach(auth()->user());
            // $user->assignRole($request->input('role_id'));

            Mail::to($user->email)->send(new UserCreated($user, $password));

            return $user;
        });

        return $user->load('manager', 'users', 'roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     * @return User
     */
    public function show(User $user): User
    {
        return $user->load('manager', 'users', 'roles');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest $request
     * @param  User $user
     * @return User
     */
    public function update(UpdateRequest $request, User $user): User
    {
        $user->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ]);
        return $user->load('manager', 'users', 'roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     * @return User
     */
    public function destroy(User $user): User
    {
        try {
            $user->delete();
        } catch (\Exception $exception) {
            return response([
                'message' => 'Cannot delete this user',
                'exception' => $exception,
            ]);
        }
        return $user->load('manager', 'users', 'roles');
    }

    public function customPassword(Request $request)
    {

        $user = User::where('email', $request->input('email'))->first();

           if($user) {
               $user->update([
                   'password' => $request->input('password'),
                   'change_password' => 0,
               ]);
               if(!$user->change_password){
                //    auth()->logout();
                   return $user->load('manager', 'users', 'roles');
               } else {
                   return response ([
                       'message' => 'change_password field was not changed',
                   ], 400);
               }
           }
           return response([
               'message' => "This user does not exists, {$request->input('email')}",
               'status' => 404
           ]);
    }
}
