<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ClientCollection;
use App\Models\Client;
use App\Http\Requests\Client\Store as StoreRequest;
use App\Http\Requests\Client\Update as UpdateRequest;
use App\Mail\ClientCreated;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class ClientsController extends Controller
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * ClientsController constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return ClientCollection
     */
    public function index(Request $request): ClientCollection
    {
        return new ClientCollection(auth()->user()->clients()->with('country', 'users', 'roles')->latest()->paginate($request->input('per_page')));
        // return new ClientCollection($this->client->with('country', 'users', 'roles')->latest()->paginate($request->input('per_page')));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return Client
     */
    public function store(StoreRequest $request): Client
    {
        // return dd($request);
        $client = DB::transaction(function () use ($request) {
            $role = Role::findByName('Cliente', 'clients');;
            $password = Str::random(10);
            $client = $this->client->create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => $request->input('generate_password') ? $password : null,
                'has_password' =>  $request->input('generate_password') ? true : false,
                'address' => $request->input('address'),
                'phone' => $request->input('phone'),
                'logo_url' => $request->input('logo_url'),
                'country_id' => $request->input('country_id')
            ])->assignRole($role);
            auth()->user()->clients()->attach($client, ['owner' => true]);

            if($request->input('generate_password')) {
                Mail::to($client->email)->send(new ClientCreated($client, $password));
            }
            return $client;
        });

        return $client->load('country', 'users', 'roles');
    }

    /**
     * Display the specified resource.
     *
     * @param Client $client
     * @return Client
     */
    public function show(Client $client): Client
    {
        return $client->load('country', 'users', 'roles');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param Client $client
     * @return Client
     */
    public function update(UpdateRequest $request, Client $client): Client
    {
        $client = DB::transaction(function () use ($request, $client) {
            $password = Str::random(10);
            $client->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => $request->input('generate_password') ? $password : null,
            'has_password' =>  $request->input('generate_password') ? true : false,
            'address' => $request->input('address'),
            'phone' => $request->input('phone'),
            'logo_url' => $request->input('logo_url'),
            'country_id' => $request->input('country_id')
        ]);
            if($request->input('assigned_users') !== null) {
                $client->users()->detach();
                auth()->user()->clients()->attach($client, ['owner' => true]);
                foreach ($request->input('assigned_users') as $assigned_user) {
                    $client->users()->attach([$assigned_user => ['owner' => false]]);
                }
            };

            if($request->input('generate_password')) {
                Mail::to($client->email)->send(new ClientCreated($client, $password));
            }

            return $client;
        });

        return $client->load('country', 'users', 'roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Client $client
     * @return Client
     */
    public function destroy(Client $client): Client
    {
        try {
             $client->delete();
             $client->users()->detach();
        } catch (\Exception $exception) {
            return response([
                'message' => 'Cannot delete this client',
                'exception' => $exception,
            ]);
        }
        return $client->load('country', 'users', 'roles');
    }
}
