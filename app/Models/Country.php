<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    protected $guarded = [];
    protected $table = 'countries';
    use SoftDeletes;

    public function clients()
    {
        return $this->hasMany(Client::class);
    }

}
