<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
// use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserCreated extends Mailable
{
    use Queueable, SerializesModels;
    protected $user, $password;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param String $password
     */
    public function __construct(User $user, String $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('user.user_created')
        ->with([
            'user' => $this->user,
            'password' => $this->password,
            'url_login' => env('URL_LOGIN')
            ])
            ->subject('Usuario creado')
            ->from(env('MAIL_FROM_ADDRESS'));
    }
}
