<?php

namespace App\Mail;

use App\Models\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ClientCreated extends Mailable
{
    use Queueable, SerializesModels;
    protected $client, $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Client $client, String $password)
    {
        $this->client = $client;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('view.name');
        return $this->view('client.client_created')
        ->with([
            'client' => $this->client,
            'password' => $this->password,
            'url_login' => env('URL_LOGIN')
            ])
            ->subject('Usuario creado')
            ->from(env('MAIL_FROM_ADDRESS'));
    }
}
