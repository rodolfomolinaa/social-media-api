<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Usuario creado</title>
</head>

<body>

<table style="height: 100%;
    border-spacing: 0;
    border-collapse: collapse;
    vertical-align: top;
    {{--background-image:url({{ asset('/img/map.png') }});--}}
    width: 100%;
    color: #0a0a0a;
    font-family: Helvetica,Arial,sans-serif;
    font-weight: normal;
    padding: 0;
    margin: 0;
    text-align: left;
    font-size: 16px;
    line-height: 1.3;">
    <tbody>
    <tr>
        <td>
            <table style="border-spacing: 0;
                            border-collapse: collapse;
                            padding: 0;
                            vertical-align: top;
                            background:white;
                            width: 580px;
                            margin: 0 auto;
                            float: none;
                            position: relative;
                            text-align: center;
                            margin-top: 40px;
                            margin-bottom: 50px;">
                <tbody>
                <tr>
                    <td>
                        <div style="text-align: center; padding-top: 0px;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding:0;">

                        <div style="padding:20px 40px 0px 40px;text-align:left;font-weight: 300;color:#7b7b7b;">
                            <span style="font-size:20px;">Hola </span><span
                                style="color:#2886fb;">{{ $client->name }}</span>,
                            <p style="color:#7b7b7b;">
                                Bienvenido a Social-Media, tu cuenta ha sido creada exitosamente.
                            </p>
                        </div>

                        <div style="margin-bottom:40px;padding:0 40px;text-align:left;font-weight: 300;color:#7b7b7b;">
                            <p>Para poder acceder a tu cuenta debes de utilizar las siguientes credenciales: <br/> <br/>
                                Correo electrónico: <span style="color:#2886fb;">{{ $client->email }}</span> <br/>
                                Contraseña: <span style="color:#2886fb;">{{ $password }}</span></p>

                            <p>Para acceder al dashboard haz click <a href="{{$url_login}}"><b>aquí</b></a> o en la
                                siguiente url:
                                <br/>
                                <a href="{{$url_login}}">http://localhost:8080/login</a>
                            </p>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>

</html>
