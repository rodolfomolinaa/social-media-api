<?php

use Illuminate\Database\Seeder;
use App\Models\Country;
use App\Models\Client;
use Illuminate\Support\Arr;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = Country::all();
        $countriesIDs = $countries->pluck('id')->toArray();

        Country::all()
            ->each(function ($country) use ($countriesIDs) {
                $country->clients()
                    ->saveMany(factory(Client::class, random_int(1, 3))
                    ->make([
                        'country_id' => Arr::random($countriesIDs),
                    ]));
            });
    }
}
