<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Spatie\Permission\Models\Role;
//use Spatie\Permission\Models\Permission;
//use App\Models\User;
use Spatie\Permission\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param Faker $faker
     * @return void
     */
    public function run(Faker $faker)
    {
        //Permission list
        Permission::create(['name' => 'roles.index']);
        Permission::create(['name' => 'roles.create']);
        Permission::create(['name' => 'roles.show']);
        Permission::create(['name' => 'roles.update']);
        Permission::create(['name' => 'roles.destroy']);

        Permission::create(['name' => 'users.index']);
        Permission::create(['name' => 'users.create']);
        Permission::create(['name' => 'users.show']);
        Permission::create(['name' => 'users.update']);
        Permission::create(['name' => 'users.destroy']);

        Permission::create(['name' => 'clients.index']);
        Permission::create(['name' => 'clients.create']);
        Permission::create(['name' => 'clients.show']);
        Permission::create(['name' => 'clients.update']);
        Permission::create(['name' => 'clients.destroy']);

        $superAdminRole = Role::create([
            'name' => 'Root',
            'description' => $faker->paragraph(),
        ]);

        $adminRole = Role::create([
            'name' => 'Administrador',
            'description' => $faker->paragraph()
        ]);

        Role::create([
            'name' => 'Publicador',
            'description' => $faker->paragraph(),
        ]);

        Role::create([
            'guard_name' => 'clients',
            'name' => 'Cliente',
            'description' => $faker->paragraph(),
        ]);

        $permissions = Permission::all();
        $superAdminRole->syncPermissions($permissions);

        $adminPermissions = Permission::all()->where('id', '>', 5);
        $adminRole->syncPermissions($adminPermissions);

    }
}
