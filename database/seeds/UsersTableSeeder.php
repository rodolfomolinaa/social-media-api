<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserCreated;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param Faker $faker
     * @return void
     */
    public function run(Faker $faker)
    {

        $superAdminRole = Role::findByName('Root');
        $password = Str::random(10);

        $rootUser = User::create([
            'name' => $faker->name,
            'email' => 'admin@email.com',
            'password' => $password,
        ]);

        $rootUser->assignRole($superAdminRole);
        Mail::to($rootUser->email)->send(new UserCreated($rootUser, $password));

//        factory(User::class, 20)->create();


//        (User::all())->each(function ($user) use ($superAdminRole, $adminRole, $publishRole) {
//            if($user->id == 1) {
//                $user->assignRole($superAdminRole);
//            } else if($user->id > 1 && $user->id <= 5) {
//                $user->assignRole($adminRole);
//            } else {
//                $user->assignRole($publishRole);
//            }
//        });
    }
}
