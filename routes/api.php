<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middleware' => 'api'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('clients/login', 'ClientsAuthController@login');

    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::post('logout', 'AuthController@logout');
        Route::post('clients/logout', 'ClientsAuthController@logout');

        Route::group(['middleware' => ['auth:users', 'role:Root|Administrador']], function () {
            Route::apiResource('users', 'UsersController');
            Route::post('/users/custom-password', 'UsersController@customPassword');

            Route::get('roles/select', 'RolesController@select');

            Route::get('countries/select', 'CountriesController@select');
            Route::apiResource('countries', 'CountriesController');

            Route::apiResource('clients', 'ClientsController');

        });

    });

});

// Route::group(['middleware' => 'auth'], function () {
//     // Route for changing password in first login
//     Route::post('/users/custom-password', 'UsersController@customPassword');

//     Route::group(['middleware' => ['role:super-admin|administrator']], function () {
//         Route::get('roles/select', 'RolesController@select');

//         //User
//         Route::apiResource('users', 'UsersController');

//         //Country
//         Route::apiResource('countries', 'CountriesController');

//         //Client
//         Route::apiResource('clients', 'ClientsController');
//     });

// });
